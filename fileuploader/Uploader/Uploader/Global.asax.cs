﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.IO;
using Serilog;
using System.Web.Http;

namespace Uploader
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var logDirectory = Server.MapPath("~/Log");
            if (!Directory.Exists(logDirectory))
                Directory.CreateDirectory(logDirectory);
            Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Information()
            .WriteTo.Console()
            .WriteTo.File(Path.Combine(logDirectory, "log.txt"),
                rollingInterval: RollingInterval.Day,
                rollOnFileSizeLimit: true)
            .CreateLogger();

            Log.Information("Application started.");
        }
    }
}
