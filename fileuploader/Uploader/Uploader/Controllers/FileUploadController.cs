﻿using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Linq;
using System.Xml;
using System;
using Serilog;
using System.Data;
using DAL;

namespace Uploader.Controllers
{
    public class FileUploadController : Controller
    {
        // GET: FileUpload
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult UploadFile()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase file)
        {
            try
            {
                var allowedExtensions = new string [] { ".xml", ".csv" };
                if (file != null && file.ContentLength > 0)
                {
                    var extension = Path.GetExtension(file.FileName);
                    if (allowedExtensions.Contains(extension))
                    {
                        switch (extension)
                        {
                            case ".xml":
                                processXMLFile(file.InputStream);
                                break;
                            case ".csv":
                                processCSVFile(file.InputStream);
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        ViewBag.Message = "Unknown format.";
                        return View("Index");
                    }
                }
                else
                {
                    ViewBag.Message = "Please select a file to upload.";
                    Log.Information("File is null.");
                }
                return View("Index");
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                ViewBag.Message = "File upload failed!";
                return View("Index");
            }
        }

        private bool processCSVFile(Stream fileStream)
        {
            bool isSuccess = false;
            try
            {
                bool isValid = true;
                var dataTable = CreateDataTable();
                string completeString;
                using (var reader = new StreamReader(fileStream))
                {
                    while (!reader.EndOfStream)
                    {
                        completeString = reader.ReadToEnd().ToString();
                        string[] rows = completeString.Split('\n');

                        foreach (string row in rows)
                        {
                            string[] rowValues = row.Split(',');
                            var dataRow = dataTable.NewRow();
                            for (int k = 0; k < rowValues.Count(); k++)
                            {
                                dataRow[k] = rowValues[k].ToString().Trim();
                            }
                            dataTable.Rows.Add(dataRow);
                        }
                    }
                    dataTable.Rows.RemoveAt(0);
                }
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    for (int j = 0; j < dataTable.Columns.Count; j++)
                    {
                        var data = dataTable.Rows[i][j].ToString();
                        if (string.IsNullOrEmpty(dataTable.Rows[i][j].ToString())
                            || String.Equals("\r", dataTable.Rows[i][j].ToString()))
                        {
                            isValid = false;
                            var dataRow = dataTable.Rows[i];
                            var datacolumn = dataTable.Columns[j];
                            Log.Information("Value missing in CSV file, transactionId :" + dataRow["TransactionID"].ToString() +
                                ", Column name:" + datacolumn.ColumnName.ToString() + ".");
                            ViewBag.Message = "Upload failed. File has invalid data.";
                        }
                    }
                }
                if (isValid)
                {
                    var transactionDataAccess = new DAL.TransactionDataAccess();
                    isSuccess = transactionDataAccess.InsertTransactionData(dataTable) > 0;
                    ViewBag.Message = "Upload Success.";
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                isSuccess = false;
                ViewBag.Message = "Upload Fail.";
            }
            return isSuccess;
        }

        private bool processXMLFile(Stream fileStream)
        {
            bool isSuccess = false;
            try
            {
                bool isValid = true;
                Log.Information("processXMLFile");
                var xmlDocument = new XmlDocument();
                xmlDocument.Load(fileStream);
                var nodeList = xmlDocument.DocumentElement.SelectNodes("/Transactions/Transaction");

                var dataTable = CreateDataTable();

                foreach (XmlNode node in nodeList)
                {
                    dataTable.Rows.Add(node.Attributes["id"].Value.Trim().ToString(),
                                node.SelectSingleNode("PaymentDetails").FirstChild.InnerText.ToString().Trim(),
                                node.SelectSingleNode("PaymentDetails").LastChild.InnerText.ToString().Trim(),
                                node.SelectSingleNode("TransactionDate").InnerText.ToString().Trim(),
                                node.SelectSingleNode("Status").InnerText.ToString().Trim()
                               );
                }

                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    for (int j = 0; j < dataTable.Columns.Count; j++)
                    {
                        if (string.IsNullOrEmpty(dataTable.Rows[i][j].ToString()))
                        {
                            isValid = false;
                            var dataRow = dataTable.Rows[i];
                            var datacolumn = dataTable.Columns[j];
                            Log.Information("Value missing in XML file, transactionId :" + dataRow["TransactionID"].ToString() +
                                ", Column name:" + datacolumn.ColumnName.ToString() + ".");
                            ViewBag.Message = "Upload failed.File has invalid data.";
                        }
                    }
                }
                if (isValid)
                {
                    var transactionDataAccess = new DAL.TransactionDataAccess();
                    isSuccess = transactionDataAccess.InsertTransactionData(dataTable) > 0;
                    ViewBag.Message = "Upload Success.";
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                isSuccess = false;
                ViewBag.Message = "Upload Fail.";
            }
            return isSuccess;
        }

        private DataTable CreateDataTable()
        {
            var dataTable = new DataTable();
            dataTable.Columns.Add("TransactionID");
            dataTable.Columns.Add("Amount");
            dataTable.Columns.Add("CurrencyCode");
            dataTable.Columns.Add("TransactionDate");
            dataTable.Columns.Add("Status");
            return dataTable;
        }
    }
}