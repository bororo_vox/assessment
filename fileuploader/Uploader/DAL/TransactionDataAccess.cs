﻿using System;
using System.Linq;
using System.Data;

namespace DAL
{
    public class TransactionDataAccess
    {
        public int InsertTransactionData(DataTable transactionsTable)
        {
            int rowsEffected = 0;
            using (var entity = new TransactionDataEntities())
            {
                for (int i = 0; i < transactionsTable.Rows.Count; i++)
                {
                    var dataRow = transactionsTable.Rows[i];
                    if (!DateTime.TryParse(dataRow["TransactionDate"].ToString(), out DateTime transactionDate))
                        transactionDate = DateTime.ParseExact(dataRow["TransactionDate"].ToString(), "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                    var transaction = new Transaction
                    {
                        TransactionId = dataRow["TransactionID"].ToString(),
                        Amount = decimal.Parse(dataRow["Amount"].ToString()),
                        CurrencyCode = dataRow["CurrencyCode"].ToString(),
                        TransactionDate = transactionDate,
                        Status = dataRow["Status"].ToString()
                    };
                    entity.Transactions.Add(transaction);
                }
                rowsEffected = entity.SaveChanges();
            }
            return rowsEffected;
        }
    }
}
